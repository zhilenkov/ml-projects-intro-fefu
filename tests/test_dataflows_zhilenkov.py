import pandas as pd

from mlintropack.dataflows.zhilenkov import JSONDataFlow, DBDataFlow

import pytest
import sqlite3
import json


INAPPROPRIATE_DICT = {
    (1, 1): 3
}

APPROPRIATE_DICT = {
    "key": "item"
}

MOCK_JSON = json.dumps({"key": "val"})

MOCK_DATAFRAME = pd.DataFrame.from_dict({"a": [1, 2, 3], "b": ["mr", "john", "doe"]})


def test_json_dataflow_save_inappropriate_dict(tmp_path):
    dataflow = JSONDataFlow(tmp_path)
    with pytest.raises(TypeError):
        dataflow.save(INAPPROPRIATE_DICT, "test.json")


def test_json_dataflow_save_load_appropriate_dict(tmp_path):
    dataflow = JSONDataFlow(tmp_path)
    dataflow.save(APPROPRIATE_DICT, "test.json")
    assert (tmp_path / "test.json").exists()
    with pytest.raises(OSError):  # Test if file not exists.
        dataflow.get_data("not_test.json")
    assert type(dataflow.get_data("test.json")) is dict


def test_db_dataflow_save_load_with_error(tmp_path):
    data_handler = DBDataFlow(tmp_path)
    with pytest.raises(sqlite3.Error):
        data_handler.save(MOCK_DATAFRAME, "john_doe")
    with pytest.raises(sqlite3.Error):
        data_handler.get_data("john_doe")


def test_db_dataflow_save_load_normal(tmp_path):
    data_handler = DBDataFlow(tmp_path / "tmp.db")
    data_handler.save(MOCK_DATAFRAME, "john_doe")
    data_handler.get_data("john_doe")
