from mlintropack.datagens.zhilenkov import LinearDataGenerator
import numpy as np


def test_linear_data_generator():
    data_generator = LinearDataGenerator(num_features=5, num_obs=100)
    data_generator.get_data(keep_in_object=True)
    assert type(data_generator.x) is np.ndarray and type(data_generator.y) is np.ndarray
    assert data_generator.x.shape == (100, 5)
    assert data_generator.y.shape == (100, 1)
