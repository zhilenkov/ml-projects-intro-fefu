from mlintropack.dataflows.dataflow import DataFlow
from pathlib import Path
from mlintropack.log import configure_logger
from sqlite3 import Error

import json
import sqlite3
import pandas as pd


class JSONDataFlow(DataFlow):
    """
    JSON data manipulator.
    """
    def __init__(self, path: Path = None):
        """
        The object instantiating with working directory 'path', which contains needed JSON file.
        Args:
            path:
        """
        self.path = path
        self.logger = configure_logger(logger_name="package")

    def get_data(self, filename: str) -> dict:
        """
        Loading json file as dictionary.

        Args:
            filename (str): Filename

        Returns: dict

        """
        if not (self.path / filename).exists():
            raise OSError("File doesn't exists.")
        with open(self.path / filename, "r") as json_file:
            data = dict(json.load(json_file))
        return data

    def save(self, data: dict, filename: str):
        """
        Saves json data with a given .

        Args:
            data (dict): Dictionary for export.
            filename (str): Export destination.

        Returns:

        """
        self.logger.debug("Starting saving process.")
        try:
            json_string = json.dumps(data, indent=4)
            with open(self.path / filename, 'w') as json_file:
                json_file.write(json_string)
        except TypeError as e:
            self.logger.error(e)
            raise TypeError

        return


class DBDataFlow(DataFlow):

    def __init__(self, db_path: Path):
        self.db_path = db_path
        self.logger = configure_logger("package")

    def get_data(self, table_name: str) -> pd.DataFrame:
        conn = None
        data = None
        try:
            conn = sqlite3.connect(str(self.db_path.absolute()))
            self.logger.debug("Connected SQLite3 DB.")
            data = pd.read_sql(f"select * from {table_name}", con=conn)
            self.logger.debug("Data loaded.")
        except Error as e:
            self.logger.error(e)
        finally:
            if conn:
                conn.close()
            else:
                raise Error

        return data

    def save(self, data: pd.DataFrame, table_name: str):

        conn = None
        try:
            conn = sqlite3.connect(str(self.db_path.absolute()))
            self.logger.debug("Connected/created SQLite3 DB.")
            data.to_sql(name=table_name, con=conn)
            self.logger.debug(f"Data saved to DB into {table_name} table.")
        except Error as e:
            self.logger.error(e)
        finally:
            if conn:
                conn.close()
            else:
                raise Error

        return


class MNISTDataLoader(DataFlow):

    def __init__(self):
        pass

    def get_data(self, *args, **kwargs):
        pass

    def save(self, *args, **kwargs):
        pass

