from abc import ABC, abstractmethod


class DataGen(ABC):

    @abstractmethod
    def get_data(self):
        pass
