import logging
import logging.config


DEFAULT_LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": "false",
    "formatters": {
        "simple": {
            "format": "%(levelname)s:  %(module)s - %(message)s"
        },
        "threading": {
            "format": "%(levelname)s: %(module)s - %(process)-4s - %(message)s"
        }
    },
    "filters": {
        "warnings_and_below": {
            "()": "mlintropack.log.filter_maker",
            "level": "WARNING"
        }
    },
    "handlers": {
        "stdout": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "simple",
            "stream": "ext://sys.stdout",
            "filters": ["warnings_and_below"]
        },
        "stderr": {
            "class": "logging.StreamHandler",
            "level": "ERROR",
            "formatter": "simple",
            "stream": "ext://sys.stderr"
        },
        "file": {
            "class": "logging.FileHandler",
            "formatter": "simple",
            "level": "DEBUG",
            "filename": "debug.log",
            "mode": "w",
            "delay": True
        },
    },
    "loggers": {
        "root": {
            "level": "NOTSET",
            "handlers": [
                "stderr",
                "stdout"
            ]
        },
        "base": {
            "level": "DEBUG",
            "handlers": [
                "stderr",
                "stdout",
                "file"
            ]
        },
        "experiment": {
            "level": "INFO",
            "handlers": [
                "stderr",
                "stdout"
            ]
        },
        "mlintropack.logger": {
            "level": "DEBUG",
            "handlers": [
                "stderr",
                "stdout"
            ]
        }
    }
}


def filter_maker(level):
    level = getattr(logging, level)

    def log_filter(record):
        return record.levelno <= level

    return log_filter


def configure_logger(logger_name: str = "package", logger_config: dict = None) -> logging.Logger:
    if logger_config is None:
        logger_config = DEFAULT_LOGGING_CONFIG
    logging.config.dictConfig(logger_config)
    logger = logging.getLogger(logger_name)
    return logger
