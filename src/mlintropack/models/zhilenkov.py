from mlintropack.models.model import Model
from sklearn.linear_model import LinearRegression


class LinearModelRegression(Model):

    def __init__(self):
        self.linear_model = LinearRegression()

    def fit(self, dataset):
        self.linear_model.fit(dataset["features"], dataset["labels"])
        return

    def predict(self, dataset):
        predictions = self.linear_model.predict(dataset["features"])
        return predictions

    def __str__(self):
        msg = f"""
        \n
        ============Linear Regression Model Report==========
        Coefficients: {self.linear_model.coef_}
        Intersept: {self.linear_model.intercept_}
        ====================================================
        \n
        """
        return msg
