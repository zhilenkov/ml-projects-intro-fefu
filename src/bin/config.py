from pathlib import Path


class PathConfiguration:
    def __init__(self):

        self.data = "data"
        self.src = "src"
        self.logs = "logs"
        self.db_data_source = "dataset.db"
        self.csv_data_source = "dataset.csv"
        self.project = Path(__file__).parent.parent.parent
        self.db = self.project / self.data / self.db_data_source
        self.file = self.project / self.data / self.csv_data_source

