from mlintropack.experiment import Experiment
from mlintropack.datagens.zhilenkov import LinearDataGenerator
from mlintropack.preprocessors.preprocessor import DoNothingPreprocessor
from mlintropack.splitter import NumpyScipySupervisedSplitter
from mlintropack.models.zhilenkov import LinearModelRegression
from mlintropack.log import configure_logger


def main():
    logger = configure_logger("experiment")
    experiment = Experiment(
        loader=LinearDataGenerator(1, 100),
        preprocess=DoNothingPreprocessor(),
        split=NumpyScipySupervisedSplitter(),
        model=LinearModelRegression(),
        saver=None,
        logger=logger,
    )
    experiment.conduct()
    print(experiment.model)


if __name__ == "__main__":
    main()
